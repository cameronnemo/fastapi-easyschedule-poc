# FastAPI + EasySchedule = DIY Cron?

## Demo

```
> python -m venv venv
> . venv/bin/activate
(venv) > pip install easyschedule fastapi uvicorn
[...]
(venv) > python -m mysvc
INFO:     Started server process [20561]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
2023-07-11 12:16:03,950 WARNING scheduled_task _run next_run_time: 2023-07-11 12:17:00.950791 - default_args: {'args': [], 'kwargs': {}}
INFO:     Uvicorn running on http://localhost:8002 (Press CTRL+C to quit)
2023-07-11 12:17:00,952 INFO _run Running task
2023-07-11 12:17:01,954 INFO _run Finished running task
2023-07-11 12:17:01,954 WARNING scheduled_task _run next_run_time: 2023-07-11 12:18:00.954406 - default_args: {'args': [], 'kwargs': {}}
2023-07-11 12:18:00,955 INFO _run Running task
2023-07-11 12:18:01,956 INFO _run Finished running task
2023-07-11 12:18:01,957 WARNING scheduled_task _run next_run_time: 2023-07-11 12:19:00.957042 - default_args: {'args': [], 'kwargs': {}}
^XINFO:     Shutting down
INFO:     Waiting for application shutdown.
INFO:     Application shutdown complete.
INFO:     Finished server process [20561]
```

## FAQ

### Is this good?

It is fast!

### Is this production quality?

It is easy!

### Will this run my code?

Probably!

## References

* <https://easyschedule.readthedocs.io/en/latest/>
* <https://fastapi.tiangolo.com/advanced/events/>
* <https://github.com/tiangolo/fastapi/issues/2713#issuecomment-768949823>
* <https://fastapi.tiangolo.com/tutorial/background-tasks>
  * <https://github.com/tiangolo/fastapi/discussions/8854>
