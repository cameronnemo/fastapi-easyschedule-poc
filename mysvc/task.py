import asyncio
import logging

from easyschedule import EasyScheduler

_SCHEDULE = '* * * * *'


async def _run():
    logging.info('Running task')
    await asyncio.sleep(1)
    logging.info('Finished running task')


async def run():
    scheduler = EasyScheduler()
    scheduler.schedule(
        _run,
        schedule=_SCHEDULE
    )
    await scheduler.start()
