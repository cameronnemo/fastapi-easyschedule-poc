import asyncio
import logging
from contextlib import asynccontextmanager

from fastapi import FastAPI

from .task import run


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s %(funcName)s %(message)s'
)


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Create the easyschedule task
    asyncio.create_task(run())
    yield


app = FastAPI(lifespan=lifespan)


@app.get("/ready")
def ready():
    return "¡Listo!"


@app.get("/live")
def live():
    return "¡Hola!"
